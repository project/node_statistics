<?php

/**
 * @file
 * The node_statistics.admin.module
 */

/**
 * Form constructor for the node_statistics_article_metrics settings form.
 *
 * @see hook_admin_settings_validate()
 *
 * @ingroup forms
 */
function node_statistic_admin_settings() {
  $form = array();
  $form['node_statistics_get_facebook'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook url'),
    '#default_value' => variable_get('node_statistics_get_facebook'),
    '#description' => t('Get URl for facebook'),
    '#required' => TRUE,
  );
  $form['node_statistics_get_delicious'] = array(
    '#type' => 'textfield',
    '#title' => t('Delicious url'),
    '#default_value' => variable_get('node_statistics_get_delicious'),
    '#description' => t('Get URl for delicious'),
    '#required' => TRUE,
  );

  $form['node_statistics_get_linkedin'] = array(
    '#type' => 'textfield',
    '#title' => t('Linkedin url'),
    '#default_value' => variable_get('node_statistics_get_linkedin'),
    '#description' => t('Get URl for linkedin'),
    '#required' => TRUE,
  );

  $form['node_statistics_to_process'] = array(
    '#type' => 'select',
    '#title' => t('Number of items to process'),
    '#options' => array('30' => 30, '60' => 60, '90' => 90, '120' => 120),
    '#default_value' => variable_get('node_statistics_to_process', 30),
    '#description' => t('Number of items to process in single batch'),
    '#required' => TRUE,
  );

  $form['node_statistics_cron'] = array(
    '#type' => 'select',
    '#title' => t('Number of days'),
    '#options' => array('24' => 1, '48' => 2, '72' => 3, '96' => 4),
    '#default_value' => variable_get('node_statistics_cron', 1),
    '#description' => t('Number of days to process cron'),
    '#required' => TRUE,
  );

  $form['node_statistics_status'] = array(
    '#type' => 'select',
    '#title' => t('Cron status'),
    '#options' => array('all' => t('All'), 'updated' => t('Updated')),
    '#default_value' => variable_get('node_statistics_status', 'updated'),
    '#description' => t('Cron status'),
    '#required' => TRUE,
  );

  $form['#validate'][] = 'node_statistics_admin_settings_validate';

  return system_settings_form($form);
}

/**
 * Form validation handler for hook_admin_settings().
 */
function node_statistics_admin_settings_validate($form, &$form_state) {
  // Nothing
}

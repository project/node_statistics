<?php

/**
 * @file
 * The node_statistics.drush.inc
 */

/**
 * Implementation of hook_drush_command().
 */
function node_statistics_drush_command() {
  $items = array();
  // The key in the $items array is the name of the command.
  $items['load-statistics'] = array(
    'description' => "Loads statistics for node",
    'arguments' => array(
      'all' => 'all , Updated',
    ),
    'examples' => array(
      'drush load-statistics all' => 'Loads all data',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );
  return $items;
}

/**
 * Call back for drush command
 */
function drush_node_statistics_load_statistics($argument) {
  drush_print('Start loading external data.' . $argument);
  _node_statistics_load($argument);
  drush_print('Now doing postprocess..');
  _node_statistics_load_data($argument);
  drush_print("Done!");
}

/**
 * Load data from external service and store in database
 */
function _node_statistics_load($argument) {
  global $base_url;
  if ($argument == 'all') {
    $result = db_truncate('node_statistics_full_data')->execute();
  }
  $query_statistics = db_select('node_statistics_full_data', 'nsfd');
  $query_statistics->distinct();
  $query_statistics->fields('nsfd', array('node_id'));

  $metrics_query = db_select('node', 'n');
  $metrics_query->fields('n', array('nid'));
  
  if ($argument == 'updated') {
    $metrics_query->condition('n.nid', $query_statistics, 'NOT IN');
  }
  $metrics_query->addTag('metricsQuery');
  $results = $metrics_query->execute();
  $result = $results->fetchAll();
  $count = 0; 
  foreach ($result as $row) {
    $node = node_load($row->nid, NULL, TRUE);
    $options = array('headers' => array('Content-Type: application/json'), 'method' => 'GET',);
    // Post url from config.
    foreach (explode(',', variable_get('node_statistics_provider')) as $provider) {
      $provider_url = variable_get('url_' . $provider);
      $url = $provider_url . $base_url . url('node/' . $node->nid);
      $command_op = drupal_http_request($url, $options);
      if ($command_op->code == 200) {
        db_insert('node_statistics_full_data')
                ->fields(array('node_id' => $node->nid, 'timestamp' => time(), 'data' => $command_op->data, 'provider' => $provider, 'node_type' => $node->type))
                ->execute();
        $count++;        
        drush_print('Loading Data For : ' . $node->nid . ' For : ' . $provider);
      }
    }
  }
  drush_print('Total records ' . $count . ' loaded');
}

function _node_statistics_load_data($argument) {
  $query_statistics = db_select('node_statistics_data', 'nsf');
  $query_statistics->distinct();
  $query_statistics->fields('nsf', array('node_id')); 

  $node_node_statistics_raw = db_select('node_statistics_full_data', 'nsfd');
  $node_node_statistics_raw->fields('nsfd', array('node_id', 'data', 'provider', 'timestamp', 'node_type'));
  if ($argument == 'updated') {
    $node_node_statistics_raw->condition('nsfd.node_id', $query_statistics, 'NOT IN');
  }
  $results = $node_node_statistics_raw->execute()->fetchAll(); 
  $count = 0;              
  foreach ($results as $row) {
    $data_provider = drupal_json_decode($row->data);
    if ($row->provider == 'delicious') {
      $data_provider = $data_provider[0];
    }
  $check_exist = db_select('node_statistics_data', 'nsd')
    ->fields('nsd', array('node_id', 'provider', 'date'))
    ->condition('nsd.node_id', $row->node_id, '=')
    ->condition('nsd.provider', $row->provider, '=')
    ->condition('nsd.date', date('Y-m-d', $row->timestamp), '=')
    ->execute()
    ->rowCount();

  if (!$check_exist && $data_provider[variable_get($row->provider)]) {
    $previous_data = db_select('node_statistics_data', 'nsd');
    $previous_data->fields('nsd', array('node_id', 'provider', 'date', 'value_raw'));
    $previous_data->addExpression('MAX(nsd.date)', 'max_date');
    $previous_data->condition('nsd.node_id', $row->node_id, '=');
    $previous_data->condition('nsd.provider', $row->provider, '=');
    $results = $previous_data->execute()->fetchAll();
    if ($results[0]->value_raw) {
      $raw = $results[0]->value_raw;
      $raw_value = $data_provider[variable_get($row->provider)] - $raw;
    }
    else {
      $raw_value = $data_provider[variable_get($row->provider)];
    } 
    if ($raw_value) {
    $inser_query = db_insert('node_statistics_data')
      ->fields(array('date' => date('Y-m-d', $row->timestamp), 'node_id' => $row->node_id,
      'provider' => $row->provider, 'value_raw' => $raw_value, 'node_type' => $row->node_type))
      ->execute();
      $count++;
      drush_print('processing full data for node ' . $row->node_id);
    } 
    else {
      drush_print('content not updated for node ' . $row->node_id);
    }
  }
  else {
      drush_print('Data not available ' . $row->node_id);
    }
  }
  drush_print('Total records ' . $count . ' processed');
}

<?php

/**
 * @file
 * The node_statistics.generate.inc
 */

function node_statistics_batch_generate(&$context) {
  global $base_url;
  // Get node total count
  $node_ids_count = db_select('node_statistics_full_data')->fields(NULL, array('node_id'))->distinct()->execute()->rowCount();

  $query_statistics = db_select('node_statistics_full_data', 'nsfd');
  $query_statistics->distinct();
  $query_statistics->fields('nsfd', array('node_id'));

  $limit = variable_get('node_statistics_to_process');
  if (empty($context['sandbox'])) {
    if (variable_get('node_statistics_to_is') == 'all') {
      $result = db_truncate('node_statistics_full_data')->execute();
    }
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $max_count_query = db_select('node', 'n');
    $max_count_query->fields('n', array('nid', 'type'));
    if ($node_ids_count && variable_get('node_statistics_to_is') == 'updated') {
      $max_count_query->condition('n.nid', $query_statistics, 'NOT IN');
    }
    $max_count = $max_count_query->execute();
    $context['sandbox']['max'] = $max_count->rowCount();
  }
  $metrics_query = db_select('node', 'n');
  $metrics_query->fields('n', array('nid'));
  $metrics_query->condition('n.nid', $context['sandbox']['current_node'], '>');
  if ($node_ids_count && variable_get('node_statistics_to_is') == 'updated') {
    $metrics_query->condition('n.nid', $query_statistics, 'NOT IN');
  }
  $metrics_query->addTag('metricsQuery');
  $metrics_query->range(0, $limit);
  $results = $metrics_query->execute();
  $result = $results->fetchAll();
  foreach ($result as $row) {
    $node = node_load($row->nid, NULL, TRUE);
    $options = array('headers' => array('Content-Type: application/json'), 'method' => 'GET',);
    // Post url from config
    foreach (explode(',', variable_get('node_statistics_provider')) as $provider) {
      $provider_url = variable_get('url_' . $provider);
      $url = $provider_url . $base_url . url('node/' . $node->nid);
      $command_op = drupal_http_request($url, $options);
      if ($command_op->code == 200) {
        db_insert('node_statistics_full_data')
                ->fields(array('node_id' => $node->nid, 'timestamp' => time(), 'data' => $command_op->data, 'provider' => $provider, 'node_type' => $node->type))
                ->execute();
        $context['results'][] = $node->nid;
        $context['sandbox']['progress']++;
        $context['sandbox']['current_node'] = $node->nid;
        $context['message'] = t('Progressing : %title (%nid)', array('%nid' => $node->nid, '%title' => check_plain($node->title)));
      }
    }
  }
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

function node_statistics_full_data_batch_process(&$context) {
  $query_statistics = db_select('node_statistics_data', 'nsf');
  $query_statistics->distinct();
  $query_statistics->fields('nsf', array('node_id')); 

  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;

    $node_node_statistics_raw = db_select('node_statistics_full_data', 'nsfd');
    $node_node_statistics_raw->fields('nsfd', array('node_id', 'data', 'provider', 'timestamp', 'node_type'));
    if (variable_get('node_statistics_to_is') == 'updated') {
      $node_node_statistics_raw->condition('nsfd.node_id', $query_statistics, 'NOT IN');
    }
    $max_count = $node_node_statistics_raw->execute()->rowCount();
    $context['sandbox']['node_count'] = $max_count;
    $context['sandbox']['max'] = $max_count;
  }
  $limit = variable_get('node_statistics_to_process');
  $node_data_query = db_select('node_statistics_full_data', 'nsfd');
  $node_data_query->fields('nsfd', array('node_id', 'data', 'provider', 'timestamp', 'node_type'));
  if (variable_get('node_statistics_to_is') == 'updated') {
      $node_data_query->condition('nsfd.node_id', $query_statistics, 'NOT IN');
  } 
  $node_data_query->range(0, $limit);
  $results = $node_data_query->execute()->fetchAll();  
                            
  foreach ($results as $row)  {
    $data_provider = drupal_json_decode($row->data);
    if ($row->provider == 'delicious') {
      $data_provider = $data_provider[0];
    }
    $check_exist = db_select('node_statistics_data', 'nsd')
                    ->fields('nsd', array('node_id', 'provider', 'date'))
                    ->condition('nsd.node_id', $row->node_id, '=')
                    ->condition('nsd.provider', $row->provider, '=')
                    ->condition('nsd.date', date('Y-m-d', $row->timestamp), '=')
                    ->execute()
                    ->rowCount();
    if (!$check_exist && $data_provider[variable_get($row->provider)]) {
      $previous_data = db_select('node_statistics_data', 'nsd');
      $previous_data->fields('nsd', array('node_id', 'provider', 'date', 'value_raw'));
      $previous_data->addExpression('MAX(nsd.date)', 'max_date');
      $previous_data->condition('nsd.node_id', $row->node_id, '=');
      $previous_data->condition('nsd.provider', $row->provider, '=');
      $results = $previous_data->execute()->fetchAll();
      if ($results[0]->value_raw) {
        $raw = $results[0]->value_raw;
        $raw_value = $data_provider[variable_get($row->provider)] - $raw;
      }
      else {
        $raw_value = $data_provider[variable_get($row->provider)];
      } 
      if ($raw_value) {
        $inser_query = db_insert('node_statistics_data')
             ->fields(array('date' => date('Y-m-d', $row->timestamp), 'node_id' => $row->node_id,
                'provider' => $row->provider, 'value_raw' => $raw_value, 'node_type' => $row->node_type))
              ->execute();
        $context['message'] = t('processing full data for %node', array('%node' => $row->node_id));
      } 
      else {
        $context['message'] = t('content not updated for %node', array('%node' => $row->node_id));
      }
    }
    else {
      $context['message'] = t('skipping node %node', array('%node' => $row->node_id));
    }
    $context['results'][] = $row->node_id;
    $context['sandbox']['progress']++;
    $context['sandbox']['node_count']--;
  }
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}
/**
 * Batch callback; finished.
 */
function node_statistics_batch_finished($success, $results, $operations) {
  // The 'success' parameter means no fatal PHP errors were detected. All
  // other error management should be handled using 'results'.
  if ($success) {
    $message = format_plural(count($results), 'One post processed.', '@count posts processed.');
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
}

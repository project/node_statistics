<?php
/**
 * @file
 * The node_statistics.module
 */

// Include drush file to get cron run.
module_load_include('inc', 'node_statistics', 'node_statistics.drush');
/**
 * Implements hook_ctools_plugin_directory -
 * This lets ctools know to scan my module for a content_type plugin file
 * Detailed docks in ctools/ctools.api.php
 */
function node_statistics_ctools_plugin_directory($owner, $plugin_type) {
  return "plugins/$plugin_type";
}

/**
 * Implementation of hook_theme()
 */
function node_statistics_theme($existing, $type, $theme, $path) {
  if ($type == 'module') {
    $templates = array(
      'node_statistics_list' => array(
        'variables' => array('statistics_list' => NULL, 'content' => NULL),
      ),
    );
    return $templates;
  }
}

/**
 * Implements hook_menu().
 */
function node_statistics_menu() {

  $items['admin/config/node-statistics'] = array(
    'title' => 'Node statistics',
    'description' => 'Node statistics settings',
    'position' => 'right',
    'weight' => 1,
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );

  $items['admin/config/node-statistics/settings'] = array(
    'title' => 'Node statistics settings',
    'description' => 'Node statistics configurations tools.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('node_statistic_admin_settings'),
    'access arguments' => array('access administration pages'),
    'weight' => 2,
    'file' => 'node_statistics.admin.inc',
  );

  $items['admin/config/node-statistics/statistics-start'] = array(
    'title' => 'Node statistics',
    'description' => 'Node statistics batch operation',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('node_statistics_form'),
    'access arguments' => array('access administration pages'),
    'weight' => 3,
  );

  return $items;
}

/**
 * hook_block_info.
 */
function node_statistics_block_info() {
  // This example comes from node.module.
  $blocks['node_statistics_site'] = array(
    'info' => t('Node statistics for site'),
    'cache' => DRUPAL_NO_CACHE,
  );
  $blocks['node_statistics_node'] = array(
    'info' => t('Node statistics for node'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * hook_block_view.
 */
function node_statistics_block_view($delta = '') {
  // This example is adapted from node.module.
  $block = array();
  switch ($delta) {
    case 'node_statistics_site':
      $block['subject'] = t('Node statistics for site');
      $block['content'] = _node_statistics_get_list();
      break;
    case 'node_statistics_node':
      $block['subject'] = t('Node statistics for node');
      $block['content'] = _node_statistics_get_list((arg(0) == 'node' && is_numeric(arg(1)) ? arg(1) : NULL));
      break;  
  }
  return $block;
}

/**
 * Function to get statistics data from database.
 */
function _node_statistics_get_list($node = NULL) {
  $content = 'chart_div';
  $query_statistics = db_select('node_statistics_data', 'nsf');
  $query_statistics->distinct();
  $query_statistics->fields('nsf', array('node_id', 'provider', 'node_type', 'date', 'value_raw'));
  $query_statistics->orderBy('nsf.date', 'DESC');// ORDER BY date.
  $query_statistics->range(0, 6);
  if ($node) {
    $query_statistics->condition('nsf.node_id', $node, '=');
    $content .= '_node_' . $node;
  } 
  $results = $query_statistics->execute()->fetchAll();
  foreach ($results as $statistics) {
    $final_statistics[$statistics->node_id][$statistics->date][$statistics->provider] = $statistics->value_raw;
  }
  return theme('node_statistics_list', array('statistics_list' => $final_statistics, 'content' => $content));
}

/**
 * theme function.
 */
function theme_node_statistics_list($variables) {
  $variable_list = "['Day', 'Facebook', 'Linkedin', 'Delicious'],";
  foreach ($variables['statistics_list'] as $key => $value) {
    foreach ($value as $k => $val) {
      $facebook = !empty($val['facebook']) ? $val['facebook'] : 0;
      $linkedin = !empty($val['linkedin']) ? $val['linkedin'] : 0;
      $delicious = !empty($val['delicious']) ? $val['delicious'] : 0;
      $variable_list .= "['" . date('d M', strtotime($k)) . " [node (" . $key . ")]'," . $facebook . "," . $linkedin . "," . $delicious . "],";
    }
  }
  drupal_add_js('https://www.google.com/jsapi', 'external');
  drupal_add_js("google.load('visualization', '1', {packages: ['corechart']});", 'inline');
  drupal_add_js("function drawVisualization() {
        // data 
        var data = google.visualization.arrayToDataTable([" . $variable_list . "]);
        var options = {
          title : 'Node Statistics Data',
          vAxis: {title: \"Like, Share, Posts\"},
          hAxis: {title: \"Day\"},
          seriesType: \"line\",
          series: {5: {type: \"line\"}}
        };
        var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      } google.setOnLoadCallback(drawVisualization);", 'inline');
  return '<div id="chart_div" class="' . $variables['content'] . '"></div>';
}

/**
 * hook_form for node statistics batch operation.
 */
function node_statistics_form() {
  $form = array();
  $form['node_statistics_to_is'] = array(
    '#type' => 'radios',
    '#title' => t('Submit content to Impact Story'),
    '#maxlength' => 10,
    '#description' => t('Option \'<b>All</b>\' will delete old data and generate new one depending on amount of content on the site, it may take a long time to complete. <br/> Option \'<b>Updated</b>\' will get only newly added content.'),
    '#options' => array('all' => t('All'), 'updated' => t('Updated')),
    '#default_value' => variable_get('node_statistics_to_is', 'Updated'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Begin'),
  );
  $form['#validate'][] = 'node_statistics_form_validate';
  return $form;
}

/**
 * hook_form_submit for node statistics form.
 */
function node_statistics_form_submit(&$form, &$form_state) {
  variable_set('node_statistics_to_is', $form_state['values']['node_statistics_to_is']);
  node_statistics_run_progressive_batch('node_statistics_batch');
}

/**
 * hook_validate for node statistics form.
 */
function node_statistics_form_validate($form, &$form_state) {
  $responce = _node_statistics_service_available_check();
  if ($responce != 200) {
    form_set_error('node_statistics', t('http://graph.facebook.com/ service unavailable'));
  }
}

/**
 * Check service is working.
 */
function _node_statistics_service_available_check() {
  $options = array('method' => 'GET');
  // Get url from config.
  // Check service working.
  $url = 'http://graph.facebook.com/http://www.drupal.org/';
  $metrics_data = drupal_http_request($url, $options);
  $responce_code = $metrics_data->code;
  return $responce_code;
}

/**
 * Run a progressive batch operation.
 */
function node_statistics_run_progressive_batch() {
  $batch = batch_get();
  if (!empty($batch)) {
    // If there is already something in the batch, don't run.
    return FALSE;
  }
  $args = func_get_args();
  $batch_callback = array_shift($args);

  if (!lock_acquire($batch_callback)) {
    return FALSE;
  }
  // Attempt to increase the execution time.
  drupal_set_time_limit(240);
  // Build the batch array.
  $batch = call_user_func_array($batch_callback, $args);
  batch_set($batch);
  // We need to manually set the progressive variable again.
  // @todo debuging only
  $batch =& batch_get();
  $batch['progressive'] = TRUE;
  // Run the batch process.
  batch_process();
  lock_release($batch_callback);
  return TRUE;
}

/**
 * Batch information callback for Generating article metrics.
 */
function node_statistics_batch() {
  $batch = array(
    'operations' => array(),
    'error_message' => t('An error has occurred.'),
    'operations' => array(
      array('node_statistics_batch_generate', array()),
      array('node_statistics_full_data_batch_process', array()),
    ),
    'finished' => 'node_statistics_batch_finished',
    'title' => t('Generating node statistics'),
    'file' => drupal_get_path('module', 'node_statistics') . '/node_statistics.generate.inc',
  );
  return $batch;
}

/**
 * Run cron.
 */
function node_statistics_cron() {
  // Default to an 1 day interval.
  $interval = variable_get('node_statistics_interval', 60 * 60 * variable_get('node_statistics_cron', 24));
  // We usually don't want to act every time cron runs (which could be every
  // day) so keep a time for the next run in a variable.
  if (time() >= variable_get('node_statistics_next_execution', 0)) {
    watchdog('node_statistics', 'node_statistics ran');
    drush_node_statistics_load_statistics(variable_get('node_statistics_status', 'updated'));
    variable_set('node_statistics_next_execution', time() + $interval);
  }
}
